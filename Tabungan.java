package com.company;

public class Tabungan {
    public int SaldoTotal = 0;

    public Tabungan(int saldo){
        this.SaldoTotal =  saldo;
    }
    public void simpanUang(int jumlah){
        this.SaldoTotal = this.SaldoTotal + jumlah;
    }
    public void ambilUang(int jumlah){
        if (jumlah < this.SaldoTotal){
            this.SaldoTotal = this.SaldoTotal-jumlah;
        }
    }

    public int getSaldo(){
        return this.SaldoTotal;
    }
    public boolean ceksaldo(int jumlah, boolean i){
        if (jumlah > this.SaldoTotal){
            return i = false;
        }
        return i;
    }
}
